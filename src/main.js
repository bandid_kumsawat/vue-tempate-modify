// Import ES6 Promise
/**
 * /
 * /username : admin pass: admin type: admin
 * /username : deaware pass: deaw1234 type: user
 */
import 'es6-promise/auto'
// Import System requirements
import Vue from 'vue'
import VueRouter from 'vue-router'
import { sync } from 'vuex-router-sync'
import routes from './routes'
import store from './store'
import VModal from 'vue-js-modal'
// Import Helpers for filters
import { domain, count, prettyDate, pluralize, DateAndTime } from './filters'

// Import Views - Top level
import AppView from '@/components/App.vue'

// Import Install and register helper items
Vue.filter('count', count)
Vue.filter('domain', domain)
Vue.filter('prettyDate', prettyDate)
Vue.filter('pluralize', pluralize)
Vue.filter('DateAndTime', DateAndTime)

Vue.use(VueRouter)

Vue.use(VModal, { dialog: true, injectModalsContainer: true })

// Routing logic
var router = new VueRouter({
  routes: routes
  // mode: 'history',
  // linkExactActiveClass: 'active',
  // scrollBehavior: function(to, from, savedPosition) {
  //   return savedPosition || { x: 0, y: 0 }
  // }
})

// Some middleware to help us ensure the user is authenticated.
// Navigation Guards
router.beforeEach((to, from, next) => {
  console.log('------ to ------')
  console.log(to)
  console.log('------ from ------')
  console.log(from)
  console.log('------ next ------')
  console.log(next)
  // console.log(to.matched.some(record => record.meta.requiresAuth)) // dependding from 'routes' meta: {requiresAuth: true}
  if (
    to.matched.some(record => record.meta.requiresAuth) && // check meta.requiresAuth === true
    // change 'state.token' in vuex
    (!router.app.$store.state.token || router.app.$store.state.token === 'null')
  ) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    window.console.log('Not authenticated')
    next({
      path: '/login',
      query: { redirect: to.fullPath }
    })
    // why bug because page 'login' call 'login' is ..bug
  } else {
    next()
  }
})

sync(store, router)  //  whenever a navigation is triggered. Guards may be resolved asynchronously

// Check local storage to handle refreshes
if (window.localStorage) {
  var localUserString = window.localStorage.getItem('user') || false
  // var localUser = JSON.parse(localUserString)
  var localUser = localUserString
  if (localUser && store.state.user !== localUser) {
    store.commit('SET_TOKEN', window.localStorage.getItem('token'))
    store.commit('SET_USER', window.localStorage.getItem('user'))
    store.commit('SET_TYPE', window.localStorage.getItem('type'))
    store.commit('SET_EMAIL', window.localStorage.getItem('email'))
    store.commit('SET_MAINPAGE', window.localStorage.getItem('mainpage'))
  } else {
    router.push('/login')
  }
}

// Start out app!
// eslint-disable-next-line no-new
new Vue({
  el: '#root',
  router: router,
  store: store,
  render: h => h(AppView)
})

// change this. demo
window.bugsnagClient = window.bugsnag('02fe1c2caaf5874c50b6ee19534f5932')
window.bugsnagClient.use(window.bugsnag__vue(Vue))
