import axios from 'axios'

export default (token) => {
  var api = axios.create({
    // baseURL: 'http://127.0.0.1:4000/api/'
    baseURL: 'https://nrwiot.pwa.co.th/api'
    // baseURL: 'http://dwdev.info:4000/api/'
    // baseURL: 'http://iotdma.info:1880/'
    // baseURL: 'http://dwdev.info:4000/api'
    // baseURL: 'http://dwdev.info:8086/'
  })
  api.defaults.headers.common['Authorization'] = 'Bearer ' + token
  return api
}
