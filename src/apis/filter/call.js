import API from '@/apis/filter/server'

var headers = {
  'Content-Type': 'application/json',
  'Accept': '*/*'
}

export default {
  provice () {
    return API().post('/aggregate/province/find', {}, headers)
  },
  provice_update (new_provice) {
    return API().put('/aggregate/province/update', new_provice, headers)
  },
  filter (branch_code) {
    return API().post('/filter/findFilter', branch_code, headers)
  },
  filter_update (filter_new) {
    return API().put('/filter/save', filter_new, headers)
  },
  filter_update_all (filter) {
    return API().post('/filter/save_all', filter, headers)
  },
  filter_delete (_id) {
    return API().delete('/filter', _id, headers)
  }
}
