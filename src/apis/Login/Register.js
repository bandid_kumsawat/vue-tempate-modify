import API from '@/apis/server'

export default {
  user (data) {
    return API().post('users', data)
  },
  find (data, headers) {
    console.log(headers)
    return API().post('/users/find', headers, data)
  },
  delete (data, headers) {
    return API().delete('/user/', headers)
  },
  update (data, headers) {
    return API().put('/user/', headers, data)
  }
}
