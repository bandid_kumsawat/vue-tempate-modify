import API from '@/apis/server'

export default {
  find (data, headers) {
    return API().post('/device/find/all', data, headers)
  },
  save (data, headers) {
    return API().post('/device/save', data, headers)
  },
  delete (data, headers) {
    return API().delete('/device/', headers)
  },
  update (data, headers) {
    return API().put('/device/', headers, data)
  }
}
