import { query } from 'influx-api'

export default {
  measurements () {
    return query({
      // http://influx.iotdma.info/query?pretty=true&u=scada&p=2020iotdmainfluxdb
      url: 'http://influx.iotdma.info',
      q: 'SHOW MEASUREMENTS',
      u: 'scada',
      p: '2020iotdmainfluxdb',
      db: 'iot',
      precision: 'ms',
      responseType: 'json'
    })
  }
}
