import API from '@/apis/Table/server'
var headers = {
  'Content-Type': 'application/json',
  'Accept': '*/*'
}

export default {
  field_branch () {
    return API().post('/aggregate/field_branch', {}, headers)
  },
  field_station (data) {
    return API().post('/aggregate/field_station', data, headers)
  },
  field_type (data) {
    return API().post('/aggregate/field_type', data, headers)
  },
  field_type_sub (data) {
    return API().post('/aggregate/field_type_sub', data, headers)
  },
  field_deviccode (data) {
    return API().post('/aggregate/field_deviccode', data, headers)
  },
  field_devicname (data) {
    return API().post('/aggregate/field_devicname', data, headers)
  },
  field_param (data) {
    return API().post('/aggregate/field_param', data, headers)
  },
  find_aggregat (data, token) {
    console.log(data)
    return API(token).post('/aggregate/find', data, headers)
  }

}
