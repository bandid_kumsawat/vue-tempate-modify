import axios from 'axios'

export default () => {
  var api = axios.create({
    baseURL: 'https://pwafuxaapi.iotdma.info'
    // baseURL: 'http://dwdev.info:4000/'
  })
  return api
}
