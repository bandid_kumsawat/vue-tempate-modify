export default {
  TOGGLE_LOADING (state) {
    state.callingAPI = !state.callingAPI
  },
  TOGGLE_SEARCHING (state) {
    state.searching = (state.searching === '') ? 'loading' : ''
  },
  SET_USER (state, user) {
    state.user = user
  },
  SET_TOKEN (state, token) {
    state.token = token
  },
  SET_TYPE (state, type) {
    state.type = type
  },
  SET_EMAIL (state, email) {
    state.email = email
  },
  SET_BRANCH_ID (state, branchid) {
    state.branch = branchid
  },
  SET_MAINPAGE (state, mainpage) {
    state.mainpage = mainpage
  }
}
